functor TigerLrValsFun(structure Token : TOKEN)
 : sig structure ParserData : PARSER_DATA
       structure Tokens : Tiger_TOKENS
   end
 = 
struct
structure ParserData=
struct
structure Header = 
struct
structure A = Absyn

end
structure LrTable = Token.LrTable
structure Token = Token
local open LrTable in 
val table=let val actionRows =
"\
\\001\000\001\000\000\000\000\000\
\\001\000\001\000\093\000\003\000\093\000\004\000\093\000\007\000\093\000\
\\009\000\093\000\011\000\024\000\012\000\023\000\013\000\022\000\
\\014\000\021\000\018\000\093\000\023\000\093\000\028\000\093\000\
\\038\000\093\000\039\000\093\000\000\000\
\\001\000\001\000\094\000\003\000\094\000\004\000\094\000\007\000\094\000\
\\009\000\094\000\011\000\024\000\012\000\023\000\013\000\022\000\
\\014\000\021\000\018\000\094\000\023\000\094\000\028\000\094\000\
\\038\000\094\000\039\000\094\000\000\000\
\\001\000\001\000\095\000\003\000\095\000\004\000\095\000\007\000\095\000\
\\009\000\095\000\011\000\024\000\012\000\023\000\013\000\022\000\
\\014\000\021\000\018\000\095\000\023\000\095\000\028\000\095\000\
\\038\000\095\000\039\000\095\000\000\000\
\\001\000\001\000\096\000\003\000\096\000\004\000\096\000\007\000\096\000\
\\009\000\096\000\011\000\024\000\012\000\023\000\013\000\022\000\
\\014\000\021\000\018\000\096\000\023\000\096\000\028\000\096\000\
\\038\000\096\000\039\000\096\000\000\000\
\\001\000\001\000\097\000\003\000\097\000\004\000\097\000\007\000\097\000\
\\009\000\097\000\011\000\024\000\012\000\023\000\013\000\022\000\
\\014\000\021\000\018\000\097\000\023\000\097\000\028\000\097\000\
\\038\000\097\000\039\000\097\000\000\000\
\\001\000\001\000\098\000\003\000\098\000\004\000\098\000\007\000\098\000\
\\009\000\098\000\011\000\024\000\012\000\023\000\013\000\022\000\
\\014\000\021\000\018\000\098\000\023\000\098\000\028\000\098\000\
\\038\000\098\000\039\000\098\000\000\000\
\\001\000\002\000\012\000\005\000\011\000\006\000\010\000\008\000\009\000\
\\017\000\008\000\026\000\007\000\036\000\006\000\037\000\005\000\
\\040\000\004\000\000\000\
\\001\000\003\000\054\000\011\000\024\000\012\000\023\000\013\000\022\000\
\\014\000\021\000\024\000\020\000\025\000\019\000\031\000\018\000\
\\032\000\017\000\033\000\016\000\034\000\015\000\038\000\014\000\000\000\
\\001\000\004\000\069\000\011\000\024\000\012\000\023\000\013\000\022\000\
\\014\000\021\000\024\000\020\000\025\000\019\000\031\000\018\000\
\\032\000\017\000\033\000\016\000\034\000\015\000\038\000\014\000\000\000\
\\001\000\007\000\053\000\011\000\024\000\012\000\023\000\013\000\022\000\
\\014\000\021\000\024\000\020\000\025\000\019\000\031\000\018\000\
\\032\000\017\000\033\000\016\000\034\000\015\000\038\000\014\000\000\000\
\\001\000\007\000\075\000\011\000\024\000\012\000\023\000\013\000\022\000\
\\014\000\021\000\024\000\020\000\025\000\019\000\031\000\018\000\
\\032\000\017\000\033\000\016\000\034\000\015\000\038\000\014\000\000\000\
\\001\000\009\000\071\000\011\000\024\000\012\000\023\000\013\000\022\000\
\\014\000\021\000\024\000\020\000\025\000\019\000\031\000\018\000\
\\032\000\017\000\033\000\016\000\034\000\015\000\038\000\014\000\000\000\
\\001\000\011\000\024\000\012\000\023\000\013\000\022\000\014\000\021\000\
\\018\000\050\000\024\000\020\000\025\000\019\000\031\000\018\000\
\\032\000\017\000\033\000\016\000\034\000\015\000\038\000\014\000\000\000\
\\001\000\011\000\024\000\012\000\023\000\013\000\022\000\014\000\021\000\
\\023\000\059\000\024\000\020\000\025\000\019\000\031\000\018\000\
\\032\000\017\000\033\000\016\000\034\000\015\000\038\000\014\000\000\000\
\\001\000\011\000\024\000\012\000\023\000\013\000\022\000\014\000\021\000\
\\024\000\020\000\025\000\019\000\028\000\068\000\031\000\018\000\
\\032\000\017\000\033\000\016\000\034\000\015\000\038\000\014\000\000\000\
\\001\000\011\000\024\000\012\000\023\000\013\000\022\000\014\000\021\000\
\\024\000\020\000\025\000\019\000\031\000\018\000\032\000\017\000\
\\033\000\016\000\034\000\015\000\038\000\076\000\000\000\
\\001\000\021\000\047\000\000\000\
\\001\000\021\000\052\000\000\000\
\\001\000\021\000\060\000\000\000\
\\001\000\024\000\046\000\000\000\
\\001\000\026\000\025\000\000\000\
\\001\000\026\000\026\000\000\000\
\\001\000\026\000\031\000\000\000\
\\001\000\026\000\051\000\000\000\
\\001\000\026\000\057\000\000\000\
\\001\000\035\000\066\000\000\000\
\\001\000\036\000\030\000\000\000\
\\001\000\038\000\065\000\000\000\
\\080\000\000\000\
\\081\000\002\000\012\000\005\000\011\000\006\000\010\000\008\000\009\000\
\\017\000\008\000\026\000\007\000\036\000\006\000\037\000\005\000\
\\040\000\004\000\000\000\
\\082\000\011\000\024\000\012\000\023\000\013\000\022\000\014\000\021\000\
\\024\000\020\000\025\000\019\000\031\000\018\000\032\000\017\000\
\\033\000\016\000\034\000\015\000\038\000\014\000\039\000\013\000\000\000\
\\083\000\011\000\024\000\012\000\023\000\013\000\022\000\014\000\021\000\
\\024\000\020\000\025\000\019\000\031\000\018\000\032\000\017\000\
\\033\000\016\000\034\000\015\000\038\000\014\000\000\000\
\\084\000\011\000\024\000\012\000\023\000\013\000\022\000\014\000\021\000\
\\024\000\020\000\025\000\019\000\031\000\018\000\032\000\017\000\
\\033\000\016\000\034\000\015\000\038\000\014\000\000\000\
\\085\000\011\000\024\000\012\000\023\000\013\000\022\000\014\000\021\000\
\\024\000\020\000\025\000\019\000\031\000\018\000\032\000\017\000\
\\033\000\016\000\034\000\015\000\038\000\014\000\000\000\
\\086\000\000\000\
\\087\000\000\000\
\\087\000\010\000\078\000\000\000\
\\088\000\021\000\028\000\022\000\027\000\000\000\
\\089\000\013\000\022\000\014\000\021\000\000\000\
\\090\000\013\000\022\000\014\000\021\000\000\000\
\\091\000\000\000\
\\092\000\000\000\
\\099\000\011\000\024\000\012\000\023\000\013\000\022\000\014\000\021\000\
\\024\000\020\000\025\000\019\000\031\000\018\000\032\000\017\000\
\\033\000\016\000\034\000\015\000\038\000\014\000\000\000\
\\100\000\011\000\024\000\012\000\023\000\013\000\022\000\014\000\021\000\
\\024\000\020\000\025\000\019\000\031\000\018\000\032\000\017\000\
\\033\000\016\000\034\000\015\000\038\000\014\000\000\000\
\\101\000\011\000\024\000\012\000\023\000\013\000\022\000\014\000\021\000\
\\024\000\020\000\025\000\019\000\031\000\018\000\032\000\017\000\
\\033\000\016\000\034\000\015\000\038\000\014\000\000\000\
\\102\000\000\000\
\\103\000\000\000\
\\104\000\000\000\
\\105\000\000\000\
\\106\000\011\000\024\000\012\000\023\000\013\000\022\000\014\000\021\000\
\\024\000\020\000\025\000\019\000\031\000\018\000\032\000\017\000\
\\033\000\016\000\034\000\015\000\038\000\014\000\039\000\055\000\000\000\
\\107\000\000\000\
\"
val actionRowNumbers =
"\030\000\031\000\047\000\021\000\
\\022\000\038\000\007\000\027\000\
\\023\000\007\000\007\000\007\000\
\\036\000\007\000\007\000\007\000\
\\007\000\007\000\007\000\007\000\
\\007\000\007\000\007\000\020\000\
\\017\000\007\000\007\000\013\000\
\\024\000\018\000\010\000\008\000\
\\029\000\050\000\004\000\003\000\
\\006\000\005\000\002\000\001\000\
\\041\000\042\000\040\000\039\000\
\\025\000\007\000\014\000\033\000\
\\048\000\019\000\007\000\007\000\
\\007\000\007\000\028\000\051\000\
\\034\000\026\000\007\000\015\000\
\\044\000\009\000\049\000\035\000\
\\007\000\012\000\007\000\007\000\
\\032\000\007\000\011\000\043\000\
\\016\000\007\000\037\000\045\000\
\\046\000\000\000"
val gotoT =
"\
\\001\000\077\000\002\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\027\000\000\000\
\\000\000\
\\000\000\
\\002\000\030\000\000\000\
\\002\000\031\000\000\000\
\\002\000\033\000\016\000\032\000\000\000\
\\000\000\
\\002\000\034\000\000\000\
\\002\000\035\000\000\000\
\\002\000\036\000\000\000\
\\002\000\037\000\000\000\
\\002\000\038\000\000\000\
\\002\000\039\000\000\000\
\\002\000\040\000\000\000\
\\002\000\041\000\000\000\
\\002\000\042\000\000\000\
\\002\000\043\000\000\000\
\\000\000\
\\000\000\
\\002\000\046\000\000\000\
\\002\000\047\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\006\000\054\000\000\000\
\\002\000\056\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\059\000\000\000\
\\002\000\060\000\000\000\
\\002\000\061\000\000\000\
\\002\000\033\000\016\000\062\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\065\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\068\000\000\000\
\\000\000\
\\002\000\070\000\000\000\
\\002\000\071\000\000\000\
\\000\000\
\\002\000\072\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\075\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\"
val numstates = 78
val numrules = 28
val s = ref "" and index = ref 0
val string_to_int = fn () => 
let val i = !index
in index := i+2; Char.ord(String.sub(!s,i)) + Char.ord(String.sub(!s,i+1)) * 256
end
val string_to_list = fn s' =>
    let val len = String.size s'
        fun f () =
           if !index < len then string_to_int() :: f()
           else nil
   in index := 0; s := s'; f ()
   end
val string_to_pairlist = fn (conv_key,conv_entry) =>
     let fun f () =
         case string_to_int()
         of 0 => EMPTY
          | n => PAIR(conv_key (n-1),conv_entry (string_to_int()),f())
     in f
     end
val string_to_pairlist_default = fn (conv_key,conv_entry) =>
    let val conv_row = string_to_pairlist(conv_key,conv_entry)
    in fn () =>
       let val default = conv_entry(string_to_int())
           val row = conv_row()
       in (row,default)
       end
   end
val string_to_table = fn (convert_row,s') =>
    let val len = String.size s'
        fun f ()=
           if !index < len then convert_row() :: f()
           else nil
     in (s := s'; index := 0; f ())
     end
local
  val memo = Array.array(numstates+numrules,ERROR)
  val _ =let fun g i=(Array.update(memo,i,REDUCE(i-numstates)); g(i+1))
       fun f i =
            if i=numstates then g i
            else (Array.update(memo,i,SHIFT (STATE i)); f (i+1))
          in f 0 handle General.Subscript => ()
          end
in
val entry_to_action = fn 0 => ACCEPT | 1 => ERROR | j => Array.sub(memo,(j-2))
end
val gotoT=Array.fromList(string_to_table(string_to_pairlist(NT,STATE),gotoT))
val actionRows=string_to_table(string_to_pairlist_default(T,entry_to_action),actionRows)
val actionRowNumbers = string_to_list actionRowNumbers
val actionT = let val actionRowLookUp=
let val a=Array.fromList(actionRows) in fn i=>Array.sub(a,i) end
in Array.fromList(List.map actionRowLookUp actionRowNumbers)
end
in LrTable.mkLrTable {actions=actionT,gotos=gotoT,numRules=numrules,
numStates=numstates,initialState=STATE 0}
end
end
local open Header in
type pos = int
type arg = unit
structure MlyValue = 
struct
datatype svalue = VOID | ntVOID of unit ->  unit
 | INT of unit ->  (int) | ID of unit ->  (string)
 | moreExp of unit ->  ( ( A.exp * int )  list)
 | seqExp of unit ->  (A.exp) | arrayExp of unit ->  (A.exp)
 | body of unit ->  (A.exp) | letExp of unit ->  (A.exp)
 | forExp of unit ->  (A.exp) | whileExp of unit ->  (A.exp)
 | ifThenElse of unit ->  (A.exp) | assign of unit ->  (A.exp)
 | inFixExp of unit ->  (A.exp) | ty of unit ->  (Symbol.symbol*int)
 | tyDec of unit ->  (A.exp) | varDec of unit ->  (A.exp)
 | exp_ of unit ->  (A.exp) | exp of unit ->  (A.exp)
 | program of unit ->  (A.exp)
end
type svalue = MlyValue.svalue
type result = A.exp
end
structure EC=
struct
open LrTable
infix 5 $$
fun x $$ y = y::x
val is_keyword =
fn (T 4) => true | (T 5) => true | (T 28) => true | (T 7) => true | 
(T 8) => true | (T 9) => true | (T 35) => true | (T 36) => true | (T 1
) => true | (T 2) => true | (T 3) => true | (T 26) => true | (T 6)
 => true | (T 34) => true | (T 29) => true | _ => false
val preferred_change : (term list * term list) list = 
(nil
,nil
 $$ (T 2))::
(nil
,nil
 $$ (T 3))::
(nil
,nil
 $$ (T 16))::
nil
val noShift = 
fn (T 0) => true | _ => false
val showTerminal =
fn (T 0) => "EOF"
  | (T 1) => "IF"
  | (T 2) => "THEN"
  | (T 3) => "ELSE"
  | (T 4) => "WHILE"
  | (T 5) => "FOR"
  | (T 6) => "DO"
  | (T 7) => "LET"
  | (T 8) => "IN"
  | (T 9) => "END"
  | (T 10) => "PLUS"
  | (T 11) => "MINUS"
  | (T 12) => "DIVIDE"
  | (T 13) => "TIMES"
  | (T 14) => "LBRACE"
  | (T 15) => "RBRACE"
  | (T 16) => "LPAREN"
  | (T 17) => "RPAREN"
  | (T 18) => "AND"
  | (T 19) => "OR"
  | (T 20) => "ASSIGN"
  | (T 21) => "LBRACK"
  | (T 22) => "RBRACK"
  | (T 23) => "EQ"
  | (T 24) => "NEQ"
  | (T 25) => "ID"
  | (T 26) => "ARRAY"
  | (T 27) => "TO"
  | (T 28) => "BREAK"
  | (T 29) => "NIL"
  | (T 30) => "GT"
  | (T 31) => "GE"
  | (T 32) => "LT"
  | (T 33) => "LE"
  | (T 34) => "OF"
  | (T 35) => "VAR"
  | (T 36) => "TYPE"
  | (T 37) => "SEMICOLON"
  | (T 38) => "COMMA"
  | (T 39) => "INT"
  | _ => "bogus-term"
local open Header in
val errtermvalue=
fn (T 25) => MlyValue.ID(fn () => ("bogus")) | 
(T 39) => MlyValue.INT(fn () => (1)) | 
_ => MlyValue.VOID
end
val terms : term list = nil
 $$ (T 38) $$ (T 37) $$ (T 36) $$ (T 35) $$ (T 34) $$ (T 33) $$ (T 32)
 $$ (T 31) $$ (T 30) $$ (T 29) $$ (T 28) $$ (T 27) $$ (T 26) $$ (T 24)
 $$ (T 23) $$ (T 22) $$ (T 21) $$ (T 20) $$ (T 19) $$ (T 18) $$ (T 17)
 $$ (T 16) $$ (T 15) $$ (T 14) $$ (T 13) $$ (T 12) $$ (T 11) $$ (T 10)
 $$ (T 9) $$ (T 8) $$ (T 7) $$ (T 6) $$ (T 5) $$ (T 4) $$ (T 3) $$ (T 
2) $$ (T 1) $$ (T 0)end
structure Actions =
struct 
exception mlyAction of int
local open Header in
val actions = 
fn (i392,defaultPos,stack,
    (()):arg) =>
case (i392,stack)
of  ( 0, ( ( _, ( MlyValue.moreExp moreExp1, _, moreExp1right)) :: _
 :: ( _, ( MlyValue.exp exp1, (expleft as exp1left), _)) :: rest671))
 => let val  result = MlyValue.program (fn _ => let val  (exp as exp1)
 = exp1 ()
 val  (moreExp as moreExp1) = moreExp1 ()
 in (A.SeqExp((exp, expleft)::moreExp))
end)
 in ( LrTable.NT 0, ( result, exp1left, moreExp1right), rest671)
end
|  ( 1, ( rest671)) => let val  result = MlyValue.program (fn _ => (
A.NilExp))
 in ( LrTable.NT 0, ( result, defaultPos, defaultPos), rest671)
end
|  ( 2, ( ( _, ( MlyValue.exp exp1, exp1left, exp1right)) :: rest671))
 => let val  result = MlyValue.program (fn _ => let val  (exp as exp1)
 = exp1 ()
 in (exp)
end)
 in ( LrTable.NT 0, ( result, exp1left, exp1right), rest671)
end
|  ( 3, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: _ :: ( _, (
 MlyValue.exp exp1, _, _)) :: _ :: ( _, ( MlyValue.ID ID1, (IDleft as 
ID1left), _)) :: rest671)) => let val  result = MlyValue.exp (fn _ =>
 let val  (ID as ID1) = ID1 ()
 val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (
A.ArrayExp({typ = Symbol.symbol ID, size = exp1, init = exp2, pos = IDleft})
)
end)
 in ( LrTable.NT 1, ( result, ID1left, exp2right), rest671)
end
|  ( 4, ( ( _, ( MlyValue.exp exp1, _, exp1right)) :: _ :: ( _, ( 
MlyValue.ID ID1, (IDleft as ID1left), _)) :: rest671)) => let val  
result = MlyValue.exp (fn _ => let val  (ID as ID1) = ID1 ()
 val  (exp as exp1) = exp1 ()
 in (A.AssignExp({var = ID, exp = exp, pos = IDleft}))
end)
 in ( LrTable.NT 1, ( result, ID1left, exp1right), rest671)
end
|  ( 5, ( ( _, ( MlyValue.exp exp1, _, exp1right)) :: _ :: ( _, ( 
MlyValue.ID ID1, _, _)) :: ( _, ( _, (VARleft as VAR1left), _)) :: 
rest671)) => let val  result = MlyValue.exp (fn _ => let val  (ID as 
ID1) = ID1 ()
 val  (exp as exp1) = exp1 ()
 in (
A.VarDec({name = Symbol.symbol ID,  typ = NONE, init = exp, pos = VARleft})
)
end)
 in ( LrTable.NT 1, ( result, VAR1left, exp1right), rest671)
end
|  ( 6, ( ( _, ( _, _, SEMICOLON1right)) :: ( _, ( MlyValue.ty ty1, _,
 _)) :: _ :: ( _, ( MlyValue.ID ID1, _, _)) :: ( _, ( _, (TYPEleft as 
TYPE1left), _)) :: rest671)) => let val  result = MlyValue.exp (fn _
 => let val  (ID as ID1) = ID1 ()
 val  (ty as ty1) = ty1 ()
 in (A.TypeDec({name = Symbol.symbol ID,ty = ty, pos = TYPEleft}))
end
)
 in ( LrTable.NT 1, ( result, TYPE1left, SEMICOLON1right), rest671)

end
|  ( 7, ( ( _, ( _, _, SEMICOLON1right)) :: ( _, ( MlyValue.exp exp1,
 (expleft as exp1left), _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  (exp as exp1) = exp1 ()
 in (A.SeqExp((exp, expleft)::[]))
end)
 in ( LrTable.NT 1, ( result, exp1left, SEMICOLON1right), rest671)
end
|  ( 8, ( ( _, ( MlyValue.ID ID1, (IDleft as ID1left), ID1right)) :: 
rest671)) => let val  result = MlyValue.exp (fn _ => let val  (ID as 
ID1) = ID1 ()
 in (A.NameTy(Symbol.symbol ID, IDleft))
end)
 in ( LrTable.NT 1, ( result, ID1left, ID1right), rest671)
end
|  ( 9, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (
A.OpExp({left = exp1, oper = A.PlusOp, right = exp2, pos = exp1left}))

end)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 10, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (
A.OpExp({left = exp1, oper = A.MinusOp, right = exp2, pos = exp1left})
)
end)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 11, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (
A.OpExp({left = exp1, oper = A.TimesOp, right = exp2, pos = exp1left})
)
end)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 12, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (
A.OpExp({left = exp1, oper = A.DivideOp, right = exp2, pos = exp1left})
)
end)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 13, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (
A.OpExp({left = exp1, oper = A.EqOp, right = exp2, pos = exp1left}))

end)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 14, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (
A.OpExp({left = exp1, oper = A.NeqOp, right = exp2, pos = exp1left}))

end)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 15, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (
A.OpExp({left = exp1, oper = A.LtOp, right = exp2, pos = exp1left}))

end)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 16, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (
A.OpExp({left = exp1, oper = A.LeOp, right = exp2, pos = exp1left}))

end)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 17, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (
A.OpExp({left = exp1, oper = A.GtOp, right = exp2, pos = exp1left}))

end)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 18, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (
A.OpExp({left = exp1, oper = A.GeOp, right = exp2, pos = exp1left}))

end)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 19, ( ( _, ( MlyValue.exp exp3, _, exp3right)) :: _ :: ( _, ( 
MlyValue.exp exp2, _, _)) :: _ :: ( _, ( MlyValue.exp exp1, _, _)) :: 
( _, ( _, (IFleft as IF1left), _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 val  exp3 = exp3 ()
 in (A.IfExp({test = exp1, then' = exp2, else' = exp3, pos = IFleft}))

end)
 in ( LrTable.NT 1, ( result, IF1left, exp3right), rest671)
end
|  ( 20, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, _, _)) :: ( _, ( _, (WHILEleft as WHILE1left), _))
 :: rest671)) => let val  result = MlyValue.exp (fn _ => let val  exp1
 = exp1 ()
 val  exp2 = exp2 ()
 in (A.WhileExp({test = exp1, body = exp2, pos = WHILEleft}))
end)
 in ( LrTable.NT 1, ( result, WHILE1left, exp2right), rest671)
end
|  ( 21, ( ( _, ( MlyValue.exp exp3, _, exp3right)) :: _ :: ( _, ( 
MlyValue.exp exp2, _, _)) :: _ :: ( _, ( MlyValue.exp exp1, _, _)) ::
 _ :: ( _, ( MlyValue.ID ID1, _, _)) :: ( _, ( _, (FORleft as FOR1left
), _)) :: rest671)) => let val  result = MlyValue.exp (fn _ => let
 val  (ID as ID1) = ID1 ()
 val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 val  exp3 = exp3 ()
 in (
A.ForExp({var = Symbol.symbol ID, lo = exp1, hi = exp2, body = exp3, pos = FORleft})
)
end)
 in ( LrTable.NT 1, ( result, FOR1left, exp3right), rest671)
end
|  ( 22, ( ( _, ( _, _, END1right)) :: _ :: ( _, ( MlyValue.exp exp2,
 _, _)) :: _ :: ( _, ( MlyValue.exp exp1, _, _)) :: _ :: ( _, ( 
MlyValue.ID ID1, _, _)) :: ( _, ( _, VARleft, _)) :: ( _, ( _, (
LETleft as LET1left), _)) :: rest671)) => let val  result = 
MlyValue.exp (fn _ => let val  (ID as ID1) = ID1 ()
 val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (
A.LetExp({decs = A.VarDec({name = Symbol.symbol ID,  typ = NONE, init = exp1, pos = VARleft}) , body = exp2 , pos = LETleft})
)
end)
 in ( LrTable.NT 1, ( result, LET1left, END1right), rest671)
end
|  ( 23, ( ( _, ( MlyValue.INT INT1, INT1left, INT1right)) :: rest671)
) => let val  result = MlyValue.exp (fn _ => let val  (INT as INT1) = 
INT1 ()
 in (A.IntExp(INT) )
end)
 in ( LrTable.NT 1, ( result, INT1left, INT1right), rest671)
end
|  ( 24, ( ( _, ( _, _, RPAREN1right)) :: ( _, ( MlyValue.exp exp1, 
expleft, _)) :: ( _, ( _, LPAREN1left, _)) :: rest671)) => let val  
result = MlyValue.exp (fn _ => let val  (exp as exp1) = exp1 ()
 in (A.SeqExp((exp, expleft)::[]))
end)
 in ( LrTable.NT 1, ( result, LPAREN1left, RPAREN1right), rest671)
end
|  ( 25, ( ( _, ( MlyValue.moreExp moreExp1, _, moreExp1right)) :: _
 :: ( _, ( MlyValue.exp exp1, (expleft as exp1left), _)) :: rest671))
 => let val  result = MlyValue.moreExp (fn _ => let val  (exp as exp1)
 = exp1 ()
 val  (moreExp as moreExp1) = moreExp1 ()
 in ((exp,expleft)::moreExp)
end)
 in ( LrTable.NT 15, ( result, exp1left, moreExp1right), rest671)
end
|  ( 26, ( ( _, ( MlyValue.exp exp1, (expleft as exp1left), exp1right)
) :: rest671)) => let val  result = MlyValue.moreExp (fn _ => let val 
 (exp as exp1) = exp1 ()
 in ((exp,expleft)::[])
end)
 in ( LrTable.NT 15, ( result, exp1left, exp1right), rest671)
end
|  ( 27, ( ( _, ( MlyValue.ID ID1, (IDleft as ID1left), ID1right)) :: 
rest671)) => let val  result = MlyValue.ty (fn _ => let val  (ID as 
ID1) = ID1 ()
 in (Symbol.symbol ID, IDleft)
end)
 in ( LrTable.NT 5, ( result, ID1left, ID1right), rest671)
end
| _ => raise (mlyAction i392)
end
val void = MlyValue.VOID
val extract = fn a => (fn MlyValue.program x => x
| _ => let exception ParseInternal
	in raise ParseInternal end) a ()
end
end
structure Tokens : Tiger_TOKENS =
struct
type svalue = ParserData.svalue
type ('a,'b) token = ('a,'b) Token.token
fun EOF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 0,(
ParserData.MlyValue.VOID,p1,p2))
fun IF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 1,(
ParserData.MlyValue.VOID,p1,p2))
fun THEN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 2,(
ParserData.MlyValue.VOID,p1,p2))
fun ELSE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 3,(
ParserData.MlyValue.VOID,p1,p2))
fun WHILE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 4,(
ParserData.MlyValue.VOID,p1,p2))
fun FOR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 5,(
ParserData.MlyValue.VOID,p1,p2))
fun DO (p1,p2) = Token.TOKEN (ParserData.LrTable.T 6,(
ParserData.MlyValue.VOID,p1,p2))
fun LET (p1,p2) = Token.TOKEN (ParserData.LrTable.T 7,(
ParserData.MlyValue.VOID,p1,p2))
fun IN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 8,(
ParserData.MlyValue.VOID,p1,p2))
fun END (p1,p2) = Token.TOKEN (ParserData.LrTable.T 9,(
ParserData.MlyValue.VOID,p1,p2))
fun PLUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 10,(
ParserData.MlyValue.VOID,p1,p2))
fun MINUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 11,(
ParserData.MlyValue.VOID,p1,p2))
fun DIVIDE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 12,(
ParserData.MlyValue.VOID,p1,p2))
fun TIMES (p1,p2) = Token.TOKEN (ParserData.LrTable.T 13,(
ParserData.MlyValue.VOID,p1,p2))
fun LBRACE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 14,(
ParserData.MlyValue.VOID,p1,p2))
fun RBRACE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 15,(
ParserData.MlyValue.VOID,p1,p2))
fun LPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 16,(
ParserData.MlyValue.VOID,p1,p2))
fun RPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 17,(
ParserData.MlyValue.VOID,p1,p2))
fun AND (p1,p2) = Token.TOKEN (ParserData.LrTable.T 18,(
ParserData.MlyValue.VOID,p1,p2))
fun OR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 19,(
ParserData.MlyValue.VOID,p1,p2))
fun ASSIGN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 20,(
ParserData.MlyValue.VOID,p1,p2))
fun LBRACK (p1,p2) = Token.TOKEN (ParserData.LrTable.T 21,(
ParserData.MlyValue.VOID,p1,p2))
fun RBRACK (p1,p2) = Token.TOKEN (ParserData.LrTable.T 22,(
ParserData.MlyValue.VOID,p1,p2))
fun EQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 23,(
ParserData.MlyValue.VOID,p1,p2))
fun NEQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 24,(
ParserData.MlyValue.VOID,p1,p2))
fun ID (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 25,(
ParserData.MlyValue.ID (fn () => i),p1,p2))
fun ARRAY (p1,p2) = Token.TOKEN (ParserData.LrTable.T 26,(
ParserData.MlyValue.VOID,p1,p2))
fun TO (p1,p2) = Token.TOKEN (ParserData.LrTable.T 27,(
ParserData.MlyValue.VOID,p1,p2))
fun BREAK (p1,p2) = Token.TOKEN (ParserData.LrTable.T 28,(
ParserData.MlyValue.VOID,p1,p2))
fun NIL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 29,(
ParserData.MlyValue.VOID,p1,p2))
fun GT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 30,(
ParserData.MlyValue.VOID,p1,p2))
fun GE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 31,(
ParserData.MlyValue.VOID,p1,p2))
fun LT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 32,(
ParserData.MlyValue.VOID,p1,p2))
fun LE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 33,(
ParserData.MlyValue.VOID,p1,p2))
fun OF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 34,(
ParserData.MlyValue.VOID,p1,p2))
fun VAR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 35,(
ParserData.MlyValue.VOID,p1,p2))
fun TYPE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 36,(
ParserData.MlyValue.VOID,p1,p2))
fun SEMICOLON (p1,p2) = Token.TOKEN (ParserData.LrTable.T 37,(
ParserData.MlyValue.VOID,p1,p2))
fun COMMA (p1,p2) = Token.TOKEN (ParserData.LrTable.T 38,(
ParserData.MlyValue.VOID,p1,p2))
fun INT (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 39,(
ParserData.MlyValue.INT (fn () => i),p1,p2))
end
end
